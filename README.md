# SingR
**Please, notice that both SingR application and this github page are still under development. Features should works, but work still in progress to improve the program. If you are not confortable with R, a .exe version for windows is available here: https://gitlab.univ-nantes.fr/E201470P/singr-desktop-application.**<br/>
SingR is a local application based on shiny, to quickly analyze single cell RNA seq data. Notice that the application is on beta version, so you may encounter some bugs. . To use local app, download the all folder, check out the "install packages" file to see which packages to install before using the app. Afterwards, lunch app by pressing "play" button on Rstudio ide (green button, top left of the screen). For each feature, you have accesse to an help button (top left of each panel).<br/>
SingR provides different tools, to analyze scRNAseq data.
## Requirements
  - Data has to be setted as Seurat Objects : https://www.rdocumentation.org/packages/Seurat/versions/3.0.1/topics/CreateSeuratObject
  - Data cannot be bigger than 4 GB. If dataset are bigger than 4GB, consider using R function "Diet seurat" to reduce it's size.
## Global interface
All panels are built on the same pattern: an input panel with customization options on the left part and a visualization panel with graph or tables on the right part. A help button is located on the input panel and gives precise instructions for each panel. All displayed figures are downloadable and sizable. Notice that figures have been modified to fit in this article and are not exactly the same as application output. Most of features can be displayed in white, black or custom colors, and metadata can be trimmed to show only part of it.
<img src="./figs/big_panel.png" width="800" height="600">
## Quality control tools
Quality control section allows to verify data integrity (number of RNAs, distributions, sample sizes ...), as it display violin plots of RNA’s among cells, and barplot to understand compositions of each metadata <br/>
##### Cell’s RNA composition
First part is dedicated to violin/box plot that shows total RNAs across groups. Those two shapes could be set by the user according to his preferences. The display option allows to switch between total RNA per cell, different RNA (genes) per cell and percent of mitochondrial RNA among all cell. Finally, group by option menu contains all the metadata previously set up by user (clusters, conditions, patient ...) and allows to show one category in particular. The y axis represent total RNA’s. The program automatically plot the y axis from minimum to maximum RNA per cells in the data set. The x axis represent groups selected in the group by option box.<br/>
<img src="./figs/violin.png" width="400" height="400">
##### Metadata composition
This panel shows composition of sub categories into metadatas.Metadata option allows to divide each metadata according to its relative composition (e.g. cellular type within each patient). Thus, scale helps to switch between percentage or relative composition. y axis shows sub categories choosed in the Metadata option box with a cell count scale or a percent of cells while x axis represent groups selected in the group by option box.<br/>
<img src="./figs/barplot.png" width="400" height="400"> 
<br/>
## Visualization tools
The next panels help to see gene expressions among metadata. It gives options to see density of genetic expression, multiple genes expressions among particular metadata or reductions visualisation.<br/>
##### Single/multi gene(s) expression
Ridge plot panel is nothing more than a quick overview of a particular gene expression in different sub groups Two options are available ((group by and classes to keep), to remove part of the metadata (e.g. show only two patients). The y axis represent different groups while the x axis is the expression level (relative expression calculated by the program and given as a numeric output) <br/>
<img src="./figs/ridge.png" width="400" height="400">
<br/>
Dot plot also helps for gene expression visualization but with multiple genes (only 5 are shown but number is not limited). It has the same other options than ridge plot part. Genes are shown on the x axis, and group on the y axis. A graphical color and size scale helps to understand the level of expression of each genes: the size of dots represents the percent expressed within the class whereas the color stands for the average expression among the data set.<br/>
<img src="./figs/dot.png" width="400" height="400">
<br/>
##### Reductions
For easy reduction visualization, our application provides a tool with 3 different visualization option: UMAP TSNE and PCA. Add label option allows to show or hide plot labels, for better clarity. If the data set have no reduction calculated, the program could calculate them, by pressing the calculate button and set the number of reduction for each one. The three visualizations features plot the first reduction on x axis and the second on y axis.<br/>
<img src="./figs/UMAP.png" width="400" height="400">
##### Genetic density expression
Those two panels provides genetic density visualization features, based on a UMAP visualization. For both plots, the first reduction stands on x axis and the second on y axis, while a color threshold allows to see areas of gene expression on the plot. For single gene expression, the app allows to see up to 4 genes at
the same time but on 4 different plots More genes would result on a unreadable figure with tiny plots, so we have limited the maximum to four.<br/>
<img src="./figs/density_1.png" width="400" height="400"> <br/>
To analyze 2 co-expressed genes(Fig 6), the program take two genes names in entry and plot it to see direct potential expression correlation.<br/>
<img src="./figs/density_2.png" width="800" height="400">
## Statistics tools
Our application can compare genetic expression between particular condition, by performing student and anova tests. It is important to remember that all statistical results should be taken carefully. Statistical tests, limits or thresholds have been set up to be easily used (p-value limit is equal to 0.05,
correction type is Bonferroni).Student test panel allows to compare different groups of cells regarding its gene expression. Chose one gene box automatically complete genes names regarding all genes in the data set provided (only one gene can be chosen). To performs statistics, user check the stats box to display a select input. It contains all the possible pairs on the plot, and the user can select as much pairs as he wants, before pressing the run button.<br/>
<img src="./figs/stat_student.png" width="400" height="400"><br/>
The ANOVA tool runs statistical comparison on a global group or sub groups. As seen before, user sets gene and group by options (Fig 3.B. Two more options allow to divide results between subgroups (split by) and remove data (isolate specific classes option)<br/>
<img src="./figs/anova.png" width="400" height="400"> <br/>
## Differential gene expression tools
Our program can perform differential calculation between two conditions, and plot the results as volcano, dotplot or heatmap, to understand difference of genetic expression. Plus, Gene ontology enrichment can be calculated with differential genes, to understand differences between two conditions regarding which biological pathways are impacted.
##### Calculation
User has to determine all parameters to estimate differential expression between user’s targets. First option (choose a method) allows to switch between 3 calculations methods: one clusters VS all clusters, multiple clusters VS multiple clusters or each clusters VS all clusters, depending on the kind of calculation the user wants to perform. Afterwards, target conditions are selected in group box(es) and precision setting could be modified. Log fold change value represent the test sensibility: the smaller it is, the more gene will be found and vice-versa. For example, the app could return hundreds of genes with a fold change around 0.01, but the calculation will take several minutes to run. A 0.8-fold change will run faster, but not much genes will be found. p value cutoff set the precision of the statistical test used. Finally, keep option allow to look up at a specific condition (e.g. only B cells). It will give differential expression among every kind of cells and it is not relevant. It is why the user has to choose one cellular type. Differential calculation returns a table, that contains genes names, log fold change values, p-values and pct.1/pct.2, which respectively stands for the percentage of cells where the feature is detected in the first and second group.
##### Volcano plot
Despite the interest of a direct visualization of all differential genes, this panel also permits to resize the differential gene table. By moving the F-value cutoff slider under the plot, the user moves two vertical lines directly on the plot (orange arrow). All dots outside both lines (red dots) will be kept, all the others (grey and green dots) will be removed. The Farthest dots from 0 are the most differential, so the most interesting to characterize clusters or sub populations. The Left part represents down regulated genes and the right part represent up regulated genes (genes that are more highly expressed in the first group). By moving f-value cutoff lines, the user removes gene with low differential expression, some of the most” uninteresting” genes. When cleaning is over, user just has to click on resize differential table button to continue, after a popup confirmation message. That procedure gives a list of top differential genes.<br/>
<img src="./figs/volcano.png" width="600" height="400"> <br/>
##### Dot plot
User can plot the top differential genes on a dot plot, to see their relative expression among a particular condition. Top differential genes have been selected on the volcano plot part.<br/>
<img src="./figs/dot.png" width="400" height="400"> <br/>
##### Heatmap
Heatmap panel offers a simple visualization of differential genes. There is no special option for this part as it is not a customizable plot. User should trim gene batch to get a readable result (go to volcano plot panel, move the f-value cutoff lines and click resize as explained before), otherwise plot will be unreadable due to a large number of genes.<br/>
<img src="./figs/heatmap_diff.png" width="600" height="400"> <br/>
## Pathways analysis tools
The application provides three ways to analyze biological pathways. The user can upload a gene set, to look at the pathway that are detailed in this data set (geneset often contains particular pathways associates with gene names). To perform gene ontology, user need a differential genes list to infer which pathways could be associated to those genes. Gene ontology runs complex algorithms, to estimate cellular pathways involved into cell populations, regarding particular gene expression. The two gene ontology methods available are enrichment based and ordered.<br/>
##### Predefined Genesets
We provide an example data table that contains 50 biological pathways, associated from several to dozens of genes (i.e. genesets). This Hallmark example data set has been uploaded on gsea-msigdb (http://www.gsea-msigdb.org/gsea/msigdb/genesets.jsp?collection=H) and pathways are various biological functions, from allograft rejection to apoptosis. This kind of calculation allows to detect differences inside a particular biological pathway. This feature is useful to understand data as biological entities, not just by looking at one or two genes. Plus, it allows to look around pathways that might not be found in gene ontology calculation.<br/>
<img src="./figs/geneset.png" width="600" height="400"> <br/>
##### Enriched gene ontology (A)
By comparing genes, markers and known cellular functions in databases, enrichGO (gene ontology over-representation test) function panel returns a pathways barchart, where physiological function are associated with differential genes. It gives biological information to the user, in a human readable way. This method only takes gene names and return physiological pathways associated, regardless of genes differential potential.<br/>
##### Ordered gene ontology (B)
gseGO (Gene Set Enrichment Gene Ontology) is another way to estimate pathways, that keeps all differential genes in the calculation, and order them by their log FC (i.e. their differential potential). GseGO take differential expression in account, in order to separate pathways and draw a more representative map. The resulting plot shows functions that are correlated to the most differential genes (i.e. the most relevant) and split it to separate enriched and depleted pathways. <br/>
<img src="./figs/GO_panel.png" width="800" height="400"> <br/>
Finally, we’ve added an option to check out a particular pathways (estimated by the previous calculation) and plot it. This representation allows to see how a biological function is actually altered or depleted. Like statistical tests, ontology results should be taken carefully. Algorithm are designed to predict cellular function based on mathematics and wet biology. These are simulations, that gives an idea about cellular batch composition, not a definitive answer.<br/>
<img src="./figs/GSEA_curve.png" width="600" height="400"> <br/>
## Nichenet
NichenetR is an R package that predict target-ligand matrix. The algorithm basically predicts relations between ligand and potential targets, based on known interactions and genes. Our program could run that calculation over three species: human mouse or rats. The feature uses 3 matrices: a ligand-target matrix that lists known relations between a gene and all its potential ligands, a ligand-receptor matrix that list relations between ligand and receptor and a weighted matrix that list relations between ligand and receptor and their weight. Afterwards, user can display information about ligand-target relation by displaying a dotplot, a heatmap or a global panel with both plots and additional information.
##### Calculation
User has to choose which group should be considered as potential sender (i.e. the ligand producer), it could be a cluster, a cellular type or any other metadata, and which group will be potential receiver (i.e. the ligand target). Afterwards, the user chooses which condition is the reference (example: cells cultivated alone) and which one is the actual condition to analyze (example : cells cultivated with activated B Cells). This choice is similar than in differential calculation: we want to know which genes are differential between two condition, to determine which ligands are found in one condition and not in another one. So we can understand our condition in a different angle, regarding ligand-target specifications. The calculation can take several minutes to run, and predictions are shown on a table. The table return a few parameters :
- test ligand: the ligand name
- Auroc, Aupr and Pearson: statistical information that make results relevant or not
- rank: the ligand position in the list 
- bonafideligand:” True” if ligand-target interaction is known and found in public‘ databases. If” False”, interaction is a prediction, based on annotation as ligand/receptor and protein-protein interaction databases.
##### Nichenet dot plot (A)
A global dotplot shows top ranked ligands within conditions chosen (here clusters on y axis). Big dots represent highly expressed genes, whereas red ones are those with the highest average expression within the batch. This representation helps to quickly see where are the most representative ligands.
##### Nichenet heatmap (B)
The heatmap feature show active target genes of top-ranked ligands. Here the user can determine which top-ranked ligands have been predicted to have a regulatory expression on differential genes.
##### Nichenet panel (C)
This global visualization of ligand activities, expression, differential expression and targeted genes is useful to understand which ligand-target have both ligand activity and differential gene expression.<br/>
<img src="./figs/nichenet.png" width="800" height="800"> <br/>
Thanks<br/>
Nathan Caceres
