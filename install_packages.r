# install Rtools from internet

install.packages("BiocManager")
install.packages("devtools")
require(devtools)
install_version("shiny", version="1.6.0",repos = "http://cran.us.r-project.org")
install.packages("shinydashboard")
install.packages("ggplot2")
install_version("tidyverse", version="1.3.1",repos = "http://cran.us.r-project.org")
BiocManager::install("org.Hs.eg.db")
BiocManager::install("org.Mm.eg.db")
BiocManager::install("org.Rn.eg.db")
BiocManager::install("ComplexHeatmap")
BiocManager::install("limma")
BiocManager::install("clusterProfiler")
BiocManager::install("MAST")
BiocManager::install("fgsea")
BiocManager::install("DOSE")
BiocManager::install("enrichplot")
BiocManager::install("pathview")
BiocManager::install("biomaRt")
BiocManager::install("dittoSeq")
install.packages("shinyjs")
install.packages("shinyBS")
install.packages("shinybusy")
install.packages("shinyalert")
install.packages("shinyFiles")
install_version("shinydashboardPlus", version="2.0.0",repos = "http://cran.us.r-project.org")
install.packages("SeuratObject")
install.packages("ggpubr")
install.packages("cowplot")
install.packages("pkgload") 
install.packages("import")
install.packages("later")
install.packages("dplyr")
install.packages("Hmisc")
install_version("sigminer", version="2.1.4",repos = "http://cran.us.r-project.org")
devtools::install_github("kevinblighe/EnhancedVolcano")
devtools::install_github("saeyslab/nichenetr")
devtools::install_github("satijalab/seurat")
